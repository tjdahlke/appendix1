# Calculates and plots the transfer function for a geophone based on the basic specs that are typically found online.
# This code calculates the Fairfield Nodal Z3000 geophone response based on the spec sheet found here:
# http://fairfieldnodal.com/media/pdfs/Z3000-Spec-Sheets-Aug2016.pdf

import numpy as np 
import matplotlib.pyplot as plt
import math
import os
import sepbase
from sepbase import *
import sys

# Notes from talking with Shuki
# omega multiplies by the 70% parameter (usually called lambda)
# omega^2 multiplies by the f0^2 parameter (usually called lambda)


# Read in the input / output filenames
eq_args_from_cmdline, args = sepbase.parse_args(sys.argv)
pars=sepbase.RetrieveAllEqArgs(eq_args_from_cmdline)
outfile1=pars["freq"]
outfile2=pars["amp"]
outfile3=pars["phs"]
seplib=pars["seplib"]
nt=int(pars["nt"])

# Z3000 Fairfield OBN parameters
La = 70;            # [#]
f0 = 10;           	# [Hz) 
SG = 59.1;    		# [V/(m/s)]
w0 = 2*math.pi*f0;  # [RAD/s]
logfmin = -4.0

# # Sjoerd parameters
# La = 68;            # [#]
# f0 = 15;           	# [Hz) 
# SG = 22.0;    		# [V/(m/s)]
# w0 = 2*math.pi*f0;  # [RAD/s]
# logfmin = -4.0

# nt=8001
dt=0.002
Nyquist=1.0/(2*dt)
dw=2*Nyquist/(nt-1)


# Make initial arrays
# logf=np.linspace(-4, 4, num=nt)
logf=np.linspace(-2,2, num=nt)
lf=np.linspace(-1.0*Nyquist, Nyquist, num=nt)

w = np.zeros(lf.size)
f = np.zeros(lf.size)
TF = np.zeros(lf.size, dtype=np.complex)
TF_amp = np.zeros(lf.size)
TF_phs = np.zeros(lf.size)


w2 = np.zeros(lf.size)
f2 = np.zeros(lf.size)
TF2 = np.zeros(lf.size, dtype=np.complex)
TF_amp2 = np.zeros(lf.size)
TF_phs2 = np.zeros(lf.size)
dB_TF_amp = np.zeros(lf.size)


for i in range(0,lf.size):
	f2[i]=10**logf[i]
	w2[i]=2*math.pi*f2[i]

	f[i]=lf[i]
	w[i]=2*math.pi*f[i]

# Compute Geophone response curves
for i in range(0,lf.size):
	TF[i]=w[i]**2.0/( -1.0*(w[i]**2) +  2.0j*w[i]*w0*La/100 + w0**2)
	TF_amp[i] = math.sqrt(TF[i].real**2 + TF[i].imag**2)
	TF_phs[i] = math.atan2(TF[i].imag , TF[i].real )#*180/math.pi;
	# dB_TF_amp = 10*math.log10(TF_amp[i]*SG);

	TF2[i]=w2[i]**2.0/( -1.0*(w2[i]**2) +  2.0j*w2[i]*w0*La/100 + w0**2)
	TF_amp2[i] = math.sqrt(TF2[i].real**2 + TF2[i].imag**2)
	TF_phs2[i] = math.atan2(TF2[i].imag , TF2[i].real )*180/math.pi;
	dB_TF_amp[i] = 10.0*math.log10(TF_amp2[i]*SG);

if (seplib in ['true', '1', 't', 'y', 'yes']):
	print('writing as seplib file\n\n')
	# Write frequency array to .H file
	f = np.require(f, dtype=np.float32)
	put_sep_axis_params(outfile1, 1, [str(lf.size), str(0.0), str(1.0), 'Frequency [Hz]'])
	sep_write_scratch(outfile1, f.byteswap())

	# Write amplitude array to .H file
	TF_amp = np.require(TF_amp, dtype=np.float32)
	put_sep_axis_params(outfile2, 1, [str(lf.size), str(-1.0*Nyquist), str(dw), 'Amplitude [?]'])
	sep_write_scratch(outfile2, TF_amp.byteswap())

	# Write phase array to .H file
	TF_phs = np.require(TF_phs, dtype=np.float32)
	put_sep_axis_params(outfile3, 1, [str(lf.size), str(-1.0*Nyquist), str(dw), 'Phase [?]'])
	sep_write_scratch(outfile3, TF_phs.byteswap())

else:
	print('writing as numpy file\n\n')

	# Write phase array to .npy file
	np.save(outfile1, f)
	# Write phase array to .npy file
	np.save(outfile2, TF_amp)
	# Write phase array to .npy file
	np.save(outfile3, TF_phs)




# #====================================================================================
# # 		Plotting the response curves
# #====================================================================================


# # dB Log plot for amplitude response
# plt.figure()
# plt.loglog(f2, TF_amp2)
# plt.xlabel('Frequency')
# plt.ylabel('Amplitude [V*s/m]')
# plt.title('Vertical geophone amplitude response')


# # degree plot for phase response
# plt.figure()
# plt.semilogx(f2,TF_phs2)
# plt.xlabel('Frequency')
# plt.ylabel('phase (degrees)')
# plt.title('Vertical geophone phase response')



# # Show impulse response in time domain
# # cc = complex(0, 1)
# # time1=np.arange(0,nt)
# # print(time1.size)
# # time2=np.fft.fftshift(time1)
# # newSigW = TF_amp2*np.exp(TF_phs2*cc)
# # newSigT = np.fft.ifft(newSigW)
# # plt.figure()
# # plt.plot(time2,newSigT)


# plt.show()



