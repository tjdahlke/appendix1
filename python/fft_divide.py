import sep2npy
import sys
import matplotlib.pyplot as plt
import numpy as np
import math
import os
import commands


eq_args_from_cmdline, args = sep2npy.parse_args(sys.argv)
pars=sep2npy.RetrieveAllEqArgs(eq_args_from_cmdline)

# Read in the filenames
ampfile=pars["amp"]
phsfile=pars["phs"]
outfile=pars["outfile"]
infile=pars["infile"]

# Read in the instrument response
ampR=np.load(ampfile)
phsR=np.load(phsfile)
# Shift the instrument response so that y[0]=0Hz, y[(n/2)-1]=-Nyquist, y[(n/2)+1]=Nyquist,y[n]=~-0.0
phsR=np.fft.fftshift(phsR)
ampR=np.fft.fftshift(ampR)

#Define some constants
eps=0.001
cc = complex(0, 1)


# Get input file header info
scratchIN=sep2npy.get_sep_his_par(infile, 'in')
formatN=sep2npy.get_sep_his_par(infile, 'data_format')
esize=sep2npy.get_sep_his_par(infile, 'esize')
assert(formatN=='xdr_float')
assert(esize=='4')
nrec=int(sep2npy.get_sep_axis_params(infile, 2)[0])
orec=float(sep2npy.get_sep_axis_params(infile, 2)[1])
drec=float(sep2npy.get_sep_axis_params(infile, 2)[2])
lrec=(sep2npy.get_sep_axis_params(infile, 2)[3])

nt=int(sep2npy.get_sep_axis_params(infile, 1)[0])
ot=float(sep2npy.get_sep_axis_params(infile, 1)[1])
dt=float(sep2npy.get_sep_axis_params(infile, 1)[2])
lt=(sep2npy.get_sep_axis_params(infile, 1)[3])

hff=sep2npy.get_sep_his_par(infile, 'hff')


# Setup the output header file
scratchpath = sep2npy.GetScratchPath()
tmp = outfile.split('/')
history_tail = tmp[len(tmp)-1]
scratchOUT = scratchpath+history_tail+'@'
cmd = "echo in=%s hff=%s >> %s" % (scratchOUT, hff, outfile)
print(cmd)
sep2npy.RunShellCmd(cmd)

# Setup output axes
sep2npy.put_sep_axis_params(outfile, 1, [str(nt), str(ot), str(dt), lt])
sep2npy.put_sep_axis_params(outfile, 2, [str(nrec), str(orec), str(drec), lrec])


# Open file handles
fin=open(scratchIN,'rb')
fout=open(scratchOUT,'wb')



#===================================================================
for irec in range(0,nrec):

	progress="percent = %f" % float(100.0*irec/nrec)
	print(progress)
	nbyte=(irec*nt*4)

	# Read 1D array from file
	fin.seek(nbyte, os.SEEK_SET)
	origSigT = np.fromfile(fin, '>f4',count=nt)

	# Do FFT on single trace of input data1
	sp = np.fft.fft(origSigT)

	# Convert to phase and amplitude
	phsD=np.arctan2(sp.imag,sp.real)
	ampD=np.sqrt(sp.imag**2 + sp.real**2)

	# Do the actual response division
	# We want to divide the frequency domain complex signals:  Ae^(phiA*i)/Be^(phiB*i) = (A/B)e^(phiA-phiB)i
	phsN=phsD-phsR
	ampN=ampD/(ampR + eps)

	# Combine into frequency domain signal
	newSigW = ampN*np.exp(phsN*cc)
	newSigT = np.fft.ifft(newSigW)

	# Write the corrected trace to file
	newSigT = np.require(newSigT, dtype=np.float32)
	newSigT=newSigT.byteswap()
	newSigT.tofile(fout)

fin.close()
fout.close()

# #-----------------------------------------------------
# # Plotting

# time1=np.arange(0,nt)
# freq = np.fft.fftfreq(nt, dt)

# # Plot the input vs output signals
# plt.figure()
# plt.plot(time1,origSigT,time1,newSigT)

# # # Plot the input data
# # fig = plt.figure(figsize=(5.0, 5.0))
# # ax = fig.add_subplot(111)
# # ax.set_title('Input data')
# # plt.imshow(origSigT.T,clim=(-2.0, 2.0))
# # ax.set_aspect('auto')

# # Plot the input signals
# plt.figure()
# plt.plot(time1,origSigT)#,time1,origSigTlast)

# # Show plots
# plt.show()
#-----------------------------------------------------



