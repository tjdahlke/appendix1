
\append{Designaturing}
\label{append:A}  


\section{From circuits to response curves}
\subsection{Hydrophones}

\par The basic circuitry of a hydrophone has a capacitor (the peizo crystal that deforms under pressure) and the inherent resistance of the rest of the circuit that can be considered as a single resistor. 

% Basic hydrophone circuit
\plot{hydro-circuit}{width=5in}{Basic circuit diagram of a hydrophone.\NR}


\par We can represent figure (\ref{fig:hydro-circuit}) with several equations that balance the current:


\begin{align}
    V_{in}-V_{out} &= Q/C \label{eq1} \\
    V_{out} &= IR \label{eq2} \\
    I &= \frac{dQ}{dt}
    \label{eq3}
\end{align}

\par Equation \ref{eq1} is the voltage on the capacitor, equation (\ref{eq2}) is the current in the resistor, and equation (\ref{eq3}) is the same current in the capacitor itself. What we are interested in is the output voltage $V_{out}$ and how it relates to the input voltage for different frequencies:
\begin{align}
    V_{out}&=f(\omega,RC)V_{in} \\
\end{align}

\noindent To derive this formulation, we begin by taking the time derivative of equation (\ref{eq1}) in order to eliminate Q:
\begin{align*}
    (V_{in}-V_{out}) &= Q/C \\
    \frac{d}{dt}(V_{in}-V_{out}) &= \frac{dQ}{dt}/C, \\
\end{align*}

\noindent 
and then combine with equation (\ref{eq3}):
\begin{align*}
    \frac{d}{dt}(V_{in}-V_{out}) &= I/C,
\end{align*}


\noindent To eliminate I, we substitute in $I=V_{out}/R$ from equation (\ref{eq2}):
\begin{align*}
    \frac{d}{dt}(V_{in}-V_{out}) &= V_{out}/RC \\
\end{align*}

\noindent To calculate $V_{in}$ from $V_{out}$, move all $V_{out}$ to the right side:
\begin{align}
    \frac{d V_{in}}{dt} - \frac{d V_{out}}{dt} &= V_{out}/RC \\
    \frac{d V_{in}}{dt} &= \frac{d V_{out}}{dt} + V_{out}/RC
    \label{diffEQ}
\end{align}

\noindent We can solve equation \ref{diffEQ} in the fourier space by making the substitution $\frac{d}{dt}=i\omega$:
\begin{align*}
    i\omega V_{in} &= i\omega V_{out} + V_{out}/RC \\
    V_{in} &= V_{out} + \frac{V_{out}}{i\omega RC} \\
    V_{in} &= V_{out}\left( 1 + \frac{1}{i\omega RC} \right) \\
    {i\omega RC}V_{in} &= V_{out}\left({i\omega RC} + 1 \right) \\
\end{align*}

\noindent This means that in Fourier space we can define our transfer function as:
\begin{align}
    f(\omega,RC) &= \frac{   {i\omega RC} }{  \left({i\omega RC} + 1\right)  } \label{transferF}
\end{align}

\noindent from which we can find the original input data by solving equation (\ref{transferF}) for $V_{in}$. From this function, we can easily find the amplitude scaling and the phase shifting that the instrument creates in the recorded data:

\begin{align}
    % A(\omega) &= \sqrt{\text{real}f(\omega,RC)^{2} + \text{imag}f(\omega,RC)^{2}} \label{amp} \\
    \phi(\omega) &= \tan^{-1} \left[ \frac{ \text{real}f(\omega,RC)}{\text{imag}f(\omega,RC)} \right]  \label{phase} \\
\end{align}



Alternatively, if we aren't interested in actually analyzing the transfer function, but just want to deconvolve our instrument response from our recorded data, we can solve for $V_{in}$ from equation \ref{diffEQ} using a finite difference approach:

\begin{align*}
    \frac{V_{in}[i]-V_{in}[i-1]}{dt} &= \frac{V_{out}[i]-V_{out}[i-1]}{dt} + \frac{V_{out}[i]+V_{out}[i-1]}{2RC} \\
    V_{in}[i] &= V_{in}[i-1] + V_{out}[i]-V_{out}[i-1] + \frac{(V_{out}[i]+V_{out}[i-1])dt}{2RC} \\
\end{align*}



\subsection{Geophones}


% Geophone circuitry
\plot{geophone-circuit-0}{width=5in}{Basic circuit diagram of a geophone.\NR}


\par For geophones, instead of having a piezo crystal modulate an input voltage, we have a mechanical system that creates voltage by converting motion of a magnet through a coil. 

\par The motion $x(t)$ that occurs in the coil produces a voltage, and of course the circuit has some general resistance to account for. However, the most significant part of the geophone response curve comes from the equations of motion for the magnet relative to the coil.

% Geophone physics
\plot{geophone-physics}{width=5in}{Diagram of internal and external static forces.\NR}


% Geophone motion
\plot{geophone-physics2}{width=5in}{Diagram of vertical component geophone responding to ground motion. $\mathbf{u}$ is the motion of the earth, and $\mathbf{x}$ is the motion of the magnet relative to the coil.\NR}


\par Figure \ref{fig:geophone-physics} shows the internal and external forces at play in a vertical component geophone, and Figure \ref{fig:geophone-physics2} shows the movement of the magnet relative to the instrument frame. We can begin our derivation of the instrument response by writing a force balance equation to balance the external (left hand side) and internal forces (right hand side).

\begin{equation}
    m\frac {\partial^{2} }{ \partial t^2 } (u + x) = -kx
\end{equation}

\noindent Where $m$ is the mass of the magnet, and $k$ is the spring constant. We can re-define $k$ according the natural frequency of the spring and mass system:

\begin{equation*}
    k=\omega_{0}^{2}m,
\end{equation*}

\noindent which gives us the equation of harmonic motion:

\begin{equation}
    \frac {\partial^{2} x }{ \partial t^2 } + \omega_{0}^{2} x = -\frac {\partial^{2} u }{ \partial t^2 }
    \label{eq:harmonic}
\end{equation}

\noindent Equation \ref{eq:harmonic} defines an undamped system, which means it would be overcome by frequencies close to the natural frequency of the system ($\omega_{0}$). In order to allow for the recording of other frequencies, we add a dampening term that is proportional to the velocity of the magnet:


\begin{equation}
    \frac {\partial^{2} x }{ \partial t^2 } + \omega_{0}^{2} x + 2\omega_{0}\lambda \frac{\partial x}{\partial t} = -\frac {\partial^{2} u }{ \partial t^2 },
    \label{eq:dampened}
\end{equation}

\noindent where $\lambda$ is the dampening ratio. A simple way to solve equation \ref{eq:dampened} is to use the Fourier transform, which allows us to subsitute $i\omega$ with $\frac{\partial}{\partial t}$. Since the actual measurement that we record is voltage, and voltage is a function of the velocity of the magnet, we ultimately would like to solve for $\frac{\partial x}{\partial t}$: 

\begin{align*}
    \frac {\partial }{ \partial t }\frac {\partial x }{ \partial t } + \omega_{0}^{2} x + 2\omega_{0}\lambda \frac{\partial x}{\partial t} &= -\frac {\partial^{2} u }{ \partial t^2 } \\
    i \omega \frac {\partial x }{ \partial t } - \frac{\omega_{0}^{2}}{i \omega} \frac {\partial x }{ \partial t } + 2\omega_{0}\lambda \frac{\partial x}{\partial t} &= \omega^{2} u \\
\end{align*}

\noindent This means we can relate the output voltage ($\frac{\partial x}{\partial t}$) with the input acceleration of the earth ($\frac{\partial^{2} u}{\partial t^{2}}$):

\begin{equation}
    \frac{\frac{\partial x}{\partial t}}{\frac{\partial^{2} u}{\partial t^2}}  = \frac{-i \omega}{{- \omega^{2}  + \omega_{0}^{2} + i 2 \omega \omega_{0}\lambda}}
    \label{eq:finalgeo}
\end{equation}






\section{De-signaturing data}

% \par From these derivations, we can solve the differential equation in equation \ref{eq:diffEQ} to get a complex curve.

% \subsection{Workflow}
\subsection{Application to Cardamom data}

\par In order to implement equations \ref{phase} and \ref{eq:finalgeo}, we perform the division in the Fourier domain. Our algorithm works according to the following workflow:

% Instrument response algorithm
	\begin{algorithm}
		\caption{Remove instrument response from data}
		\label{response_removal}
		\begin{algorithmic}[1]
			\Procedure{ResponseRemoval}{$data$,$response$}
			\For {each trace $i$ in $data$}
			
			\State $temp(i) = \text{FFT}(data(i)) $
			
			\State $temp(i).amp = \sqrt{ \left( temp(i).imag \right)^{2} + \left( temp(i).real \right)^{2}  }$
			
			\State $temp(i).phase = \tan^{-1} \left( \frac{temp(i).imag}{temp(i).real} \right) $
			
			\State $temp2(i).phase = temp(i).phase - response.phase$
			
			\State $temp2(i).amp = \frac{temp(i).amp}{response.amp }$
			
			\State $ output(i) = \text{FFT}^{-1}(temp2(i)) $

			\EndFor
			\State Return $output$
			\EndProcedure
		\end{algorithmic}
	\end{algorithm}
 
 
\par We perform the actual instrument response removal in the Fourier domain, and then convert back to the time domain. The response curves are functions of frequency as we describe in equations \ref{eq:dampened} and \ref{eq:finalgeo}. We can see the impact of the  response removal in Figure \ref{fig:hydro-response-removal-comp} and Figure \ref{fig:geophone-response-removal-comp}. Further, the difference in the spectra can be seen clearly in Figures \ref{fig:spectra-vert-compare} and \ref{fig:spectra-hydr-compare}, where the lower frequencies are noticably boosted.


\plot{hydro-response-removal-comp}{width=5in}{Hydrophone data original (left), designatured (middle) and difference (right). Clipped to emphasize differences.\ER}


\plot{geophone-response-removal-comp}{width=5in}{Geophone data original (left), designatured (middle) and difference (right). Clipped to emphasize differences.\ER}


\plot{spectra-vert-compare}{width=5in}{Geophone data spectra before response removal (left), and after (right) for each trace.\ER}


\plot{spectra-hydr-compare}{width=5in}{Hydrophone data spectra before response removal (left), and after (right) for each trace.\ER}




