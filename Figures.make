#==========================================================================================================
#
#		SINGLE GATHER TESTING
#
#==========================================================================================================

#	This is for processing file #90, which contains nodeID #604. This will need to be
#	changed based on where the data is stored. I was not given permission to share the data 
#	with this repo, so it is only accessible to members of the Stanford Exploration Project (SEP). 

testHydr=${hydr}/CARD_HYDR_full-90.H
testVert=${vert}/CARD_VERT_full-90.H
nodeid=604
#284

#-------------------------------------------------
# Make single receiver hydrophone gather
singleRecHydr%.H:
	Window_key synch=1 key1=nodeid mink1=$(shell echo $*-1.0 | bc) \
	maxk1=$(shell echo $*+1.0 | bc) \
	< ${testHydr} > $@ hff=$@@@

sortedHydr%.H: singleRecHydr%.H
	Sort3d nkeys=1 key1=offset ng1=1600 dg1=10.0 og1=0.0 < $< > $@1
	Stack3d < $@1 > $@
	echo "label1=Time[s] label2=Offset[m]" >> $@
	rm $@1

#-------------------------------------------------
# Make single receiver geophone gather
singleRecVert%.H:
	Window_key synch=1 key1=nodeid mink1=$(shell echo $*-1.0 | bc) \
	maxk1=$(shell echo $*+1.0 | bc) \
	< ${testVert} > $@

sortedVert%.H: singleRecVert%.H
	Sort3d nkeys=1 key1=offset ng1=1600 dg1=10.0 og1=0.0 < $< > $@1
	Stack3d < $@1 > $@
	echo "label1=Time[s] label2=Offset[m]" >> $@
	rm $@1


#---------------------------------------------------------------------------------
# Designature the data

# Hydrophone response curves
hydrophone_response_AMP.npy hydrophone_response_PHS.npy: 
	python ${PY}/hydrophone_response.py nt=${nn1} seplib=no freq=f.npy amp=hydrophone_response_AMP.npy phs=hydrophone_response_PHS.npy

# Geophone response curves
geophone_response_AMP.npy geophone_response_PHS.npy: 
	python ${PY}/geophone_response.py nt=${nn1} seplib=no freq=f.npy amp=geophone_response_AMP.npy phs=geophone_response_PHS.npy

${hydr}/%.H.desig: hydrophone_response_AMP.npy hydrophone_response_PHS.npy
	Window3d synch=1 <  ${hydr}/$*.H | Pad beg1=${fftpad} end1=${fftpad} > $@1
	python ${PY}/fft_divide.py infile=$@1 amp=hydrophone_response_AMP.npy \
	phs=hydrophone_response_PHS.npy outfile=$@2
	Window3d synch=1 < $@2 f1=${fftpad} n1=7001 > $@
	echo "label1='Time[s]' label2='Offset[m]'" >> $@
	rm $@1 $@2

${vert}/%.H.desig: geophone_response_AMP.npy geophone_response_PHS.npy
	Window3d synch=1 <  ${vert}/$*.H | Pad beg1=${fftpad} end1=${fftpad} > $@1
	python ${PY}/fft_divide.py infile=$@1 amp=geophone_response_AMP.npy \
	phs=geophone_response_PHS.npy outfile=$@2
	Window3d synch=1 < $@2 f1=${fftpad} n1=7001 > $@
	echo "label1='Time[s]' label2='Offset[m]'" >> $@
	rm $@1 $@2

hydrophone_fftdivide.H: sortedHydr${nodeid}.H hydrophone_response_AMP.npy hydrophone_response_PHS.npy
	Window3d synch=1 <  $< | Pad beg1=${fftpad} end1=${fftpad} > $@1
	python ${PY}/fft_divide.py infile=$@1 amp=hydrophone_response_AMP.npy \
	phs=hydrophone_response_PHS.npy outfile=$@2
	Window3d synch=1 < $@2 f1=${fftpad} n1=7001 > $@
	echo "label1='Time[s]' label2='Offset[m]'" >> $@
	rm $@1 $@2

geophone_fftdivide.H: sortedVert${nodeid}.H geophone_response_AMP.npy geophone_response_PHS.npy
	Window3d synch=1 <  $< | Pad beg1=${fftpad} end1=${fftpad} > $@1
	python ${PY}/fft_divide.py infile=$@1 amp=geophone_response_AMP.npy \
	phs=geophone_response_PHS.npy outfile=$@2
	Window3d synch=1 < $@2 f1=${fftpad} n1=7001 > $@
	echo "label1='Time[s]' label2='Offset[m]'" >> $@
	rm $@1 $@2


#==========================================================================================================
#==========================================================================================================

# Hydrophone desigature comparison
hydrgrey= title=' ' wantscalebar=y pclip=100 newclip=1 bclip=-30.0 eclip=30.0

${R}/hydro-response-removal-comp.v: hydrophone_fftdivide.H
	Bandpass flo=1.0 < $< > $@b
	< $@b Grey ${hydrgrey} ${FIG}$@2
	Add scale=1,-1 $@b sortedHydr${nodeid}.H | Grey title=' ' wantscalebar=y ${FIG}$@3
	< sortedHydr${nodeid}.H Grey ${hydrgrey} ${FIG}$@1
	vp_SideBySideAniso $@1 $@2 $@3 > $@
	rm $@1 $@2 $@3 $@b


# Geophone desigature comparison
vertgrey=title=' ' wantscalebar=y pclip=100 newclip=1 bclip=-30.0 eclip=30.0 

${R}/geophone-response-removal-comp.v: geophone_fftdivide.H
	Bandpass flo=1.0 < $< > $@b
	< $@b Grey ${vertgrey} ${FIG}$@2
	Add scale=1,-1 $@b sortedVert${nodeid}.H | Grey title=' ' wantscalebar=y ${FIG}$@3
	< sortedVert${nodeid}.H  Grey ${vertgrey} ${FIG}$@1
	vp_SideBySideAniso $@1 $@2 $@3 > $@
	rm $@1 $@2 $@3 $@b


# Hydrophone Spectra figures
${R}/Spectra_hydr_before.v: sortedHydr${nodeid}.H
	Spectra < $< | Graph max1=20.0 title='Hydrophone spectra: before designature' label1='Hz' label2='Amplitude' ${FIG}$@

${R}/Spectra_hydr_after.v: hydrophone_fftdivide.H
	Spectra < $< | Graph max1=20.0 title='Hydrophone spectra: after designature' label1='Hz' label2='Amplitude' ${FIG}$@

${R}/spectra-hydr-compare.v: ${R}/Spectra_hydr_before.v ${R}/Spectra_hydr_after.v
	vp_SideBySideAniso $^ > $@

# Geophone Spectra figures
${R}/Spectra_vert_before.v: sortedVert${nodeid}.H
	Spectra < $< | Graph max1=20.0 title='Geophone spectra: before designature' label1='Hz' label2='Amplitude' ${FIG}$@

${R}/Spectra_vert_after.v: geophone_fftdivide.H
	Spectra < $< | Graph max1=20.0 title='Geophone spectra: after designature' label1='Hz' label2='Amplitude' ${FIG}$@

${R}/spectra-vert-compare.v: ${R}/Spectra_vert_before.v ${R}/Spectra_vert_after.v
	vp_SideBySideAniso $^ > $@

#==========================================================================================================
#==========================================================================================================

burn:
	rm -f *.su
	rm -f *.H*

%.pdf: %.v
	pstexpen $< $@1 color=y fat=1 fatmult=1.5 invras=y
	epstopdf $@1
	rm $@1 

