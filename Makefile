


#VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
#VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
#
#		APPENDIX A:  Designaturing the cardamom data
#			This requires that you already have the data created using the 
#			cardamom_data repo. You will likely only have access to that data
#			if you are a member of SEP.
#
#VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
#VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV



main=$(shell pwd)
include ${SEPINC}/SEP.top
include Figures.make


setdatapaths:
	mkdir -p /data3/cardamom_working/${USER}/scratch/
	echo "datapath=/data3/cardamom_working/${USER}/scratch/" > .datapath


#VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV

NR: 
	cp FigNR/hydro-circuit.pdf Fig/.
	cp FigNR/geophone-circuit-0.pdf Fig/.
	cp FigNR/geophone-physics.pdf Fig/.
	cp FigNR/geophone-physics2.pdf Fig/.


ER: Fig/spectra-vert-compare.pdf  Fig/spectra-hydr-compare.pdf   \\
Fig/geophone-response-removal-comp.pdf Fig/hydro-response-removal-comp.pdf


CR: # No CR figures


EXE: # No EXE programs, all python scripts


#VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV



